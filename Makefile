#Makefile
#define variables
objects= main.o Shader.o Camera.o Surf.o
CPP= g++                 #c++ compiler
opt= -O2             #optimization flag
#LIBS= -lglut -lGLU -lGL  
#LIBS= -lGLU -lGL -lGLEW -lm -lsfml-graphics -lsfml-window -lsfml-system 
LIBS= -lGLU -lGL -lGLEW -lsfml-graphics -lsfml-window -lsfml-system
execname= main


build: $(objects)
	$(CPP) $(opt) -o $(execname) $(objects) $(LIBS)

Shader.o: Shader.cpp
	$(CPP) $(opt) -c Shader.cpp

Camera.o: Camera.cpp
	$(CPP) $(opt) -c Camera.cpp

Surf.o: Surf.cpp
	$(CPP) $(opt) -c Surf.cpp

main.o: main.cpp
	$(CPP) $(opt) -c main.cpp


clean:
	rm main.o
	rm Shader.o
	rm Camera.o
	rm Surf.o
