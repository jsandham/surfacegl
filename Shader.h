#ifndef __SHADER_H__
#define __SHADER_H__

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include <GL/glew.h>



class Shader
{
	public:
		GLuint Program;
		Shader(const GLchar* vertexShaderPath, const GLchar* fragmentShaderPath);
		void use();

		GLint getAttrib(const char *name);
		GLint getUniform(const char *name);

};

#endif