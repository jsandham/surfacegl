#include <iostream>

#include "Surf.h"
#include <math.h>


int main()
{
	Surf mysurface("vertex.vs", "fragment.frag");
	int N = 256;
	float graph[256][256];

	int t=0;
	while(t <= 10000){
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				float x = (i - N / 2) / (N / 2.0);
				float y = (j - N / 2) / (N / 2.0);
				float d = hypotf(x, y) * 4.0;
				float z = sin(0.01*t)*(1 - d * d) * expf(d * d / -2.0);
				graph[i][j] = roundf(z * 127 + 128)/64.0;
				
				// float x = -1.0 + 2.0*i/N;
				// float y = -1.0 + 2.0*j/N;
				// graph[i][j] = (cos(0.02*t)*x*x + sin(0.01*t)*y*y + 1)/3.0;

				// if((i > 0 && i<3*N/4) && (j > 0 && j<3*N/4)){
				// 	graph[i][j] = 0.75;
				// }
				// else{
				// 	graph[i][j]= 0.0;
				// }
				if(t==100){
					//std::cout << graph[i][j] << " ";
				}
			}
			if(t==100){
				//std::cout << "" << std::endl;
			}
		}


		//std::cout<<graph<<std::endl;
		mysurface.draw(&graph[0][0], N, N, 0 ,4);
		t++;
	}
}
